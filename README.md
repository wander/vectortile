## VectorTiles (HDRP)

Can download a vector tile from a TMS service and
can convert the tile to a raster for basing a terrain upon it.


### Dependencies (GIT Pacakages)
- utils

### Usage:


```
bool cancelToken = false; // So that it can cancel when called from different thread.
var tile = VectorTile.FromUrl( url );
byte[] controlMap = tile.RenderToTextureSingle(
	int resolution = 256,
	out Dictionary<byte, byte> remappedLayerIndices,     
	out List<Vector3Int> failedPixels,
	ref bool cancelToken );


```

